package edu.byui.lusk;

import java.util.*;

public class understandingExceptionHandling {

    public static void main(String[] args) {

        System.out.println("\n-- Call to method WITHOUT error --\n");

        addIntsToArray(4,4);

        System.out.println("\n-- Call to method WITH error --\n");

        // Call to method with error
        addIntsToArray(4,5);
    }


    //method to add numbers to array to show successful catch
    // array Maximum could exceed arraySize, causing an error
    public static void  addIntsToArray(int arraySize, int arrayMaximum){
        for (int i = 0; i < arrayMaximum; i++) {

            // initialize array with size
            int [] intArray = new int[arraySize];

            //checks if "i" exceeds array size
            try {
                System.out.println("enter try block for " + i);
                intArray[i] = i;
                System.out.println("successful try");
            } catch (Exception e) {
                //response to Exception of exceeding array size
                System.out.println("Error caught. Size of array exceeded.");
            }

        }
    }
}
