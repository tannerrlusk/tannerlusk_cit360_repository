package edu.byui.lusk;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GrowingPots {

    public static void main(String[] args) {
        //Declare variable
        Scanner input = new Scanner(System.in);
        int numPlants = 0;
        int plantSelector = 1;
        int numPots = 1;
        boolean correctInput = false;
        final String[] herbList = {"Oregano", "Parsley" , "Cilantro" , "Rosemary" , "Basil"};
        List<Runnable> herbPlants = new ArrayList<Runnable>();

        //Explanation of program
        System.out.println("This program will allow the user to plant herbs and grow them based on available pots.\n" +
                           "Please note that an increased number of plants will result in increased time required\n" +
                           "to complete. Also, an increased number of pots will result in an increased strain on\n" +
                           "your CPU.\n");

        //Gather input from user
        System.out.println("Please enter the number of plants you would like to plant: ");
        do {
            try {
                numPlants = input.nextInt();
                if (numPlants > 0){
                    correctInput = true;
                }
                else {
                    System.out.println("Invalid Input. Please enter a number greater than 0");
                    input.nextLine();
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid Input. Please enter a whole number: ");
                input.nextLine();
            }
        }while(!correctInput);



        //Gather input from user
        System.out.println("Enter '1' for Oregano, '2' for Parsley, '3' for Cilantro, '4' for Rosemary" +
                           " or '5' for Basil.");

        for (int i = 1; i <= numPlants; i++) {
            //Reset Boolean
            correctInput = false;

            //User correctly enters which herb
            System.out.println("What should plant " + i + " be?");
            do {
                try {
                    plantSelector = input.nextInt();
                    if (plantSelector > 0 && plantSelector <= 5) {
                        correctInput = true;
                    } else {
                        System.out.println("Invalid Input. Please enter a number between 1 and 5: ");
                        input.nextLine();
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Invalid Input. Please enter a whole number between 1 and 5: ");
                    input.nextLine();
                }
            } while (!correctInput);


            //Create Runnable Herb object
            herbPlants.add(new Herb(herbList[plantSelector - 1]));
        }

        //Reset Boolean
        correctInput = false;

        //User correctly enters number of pots
        System.out.println("How many pots will you use?");
        do {
            try {
                numPots = input.nextInt();
                if (numPots > 0) {
                    correctInput = true;
                } else {
                    System.out.println("Invalid Input. Please enter a whole number greater than zero: ");
                    input.nextLine();
                }
            } catch (InputMismatchException e) {
                System.out.println("Invalid Input. Please enter a whole number: ");
                input.nextLine();
            }
        } while (!correctInput);

        //Initiate ExecutorService implementation with user determined number of threads
        ExecutorService pot = Executors.newFixedThreadPool(numPots);

        //Execute threads
        for (int i = 0 ; i < herbPlants.size(); i++)
        pot.execute(herbPlants.get(i));
        pot.shutdown();


    }
}
