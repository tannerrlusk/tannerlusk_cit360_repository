package edu.byui.lusk;

public class Pizza {
    private int pNumber;
    private String pSize;
    private String topping1;
    private String topping2;
    private String topping3;
    private String topping4;
    private double price;

    public Pizza(int pNumber, String pSize, String topping1, String topping2, String topping3, String topping4, double basePrice){
        this.pNumber = pNumber;
        this.pSize = pSize;
        this.topping1 = topping1;
        this.topping2 = topping2;
        this.topping3 = topping3;
        this.topping4 = topping4;
        if (topping1.equals("none")){
            this.price = basePrice;
        }
        else if (topping2.equals("none")){
            this.price = basePrice + 1.95;
        }
        else if (topping3.equals("none")){
            this.price = basePrice + 3.90;
        }
        else if (topping4.equals("none")){
            this.price = basePrice + 5.85;
        }
        else {
            this.price = basePrice + 7.80;
        }

    }

    public int getpNumber() {
        return pNumber;
    }

    public void setpNumber(int pNumber) {
        this.pNumber = pNumber;
    }

    public String getpSize() {
        return pSize;
    }

    public void setpSize(String pSize) {
        this.pSize = pSize;
    }

    public String getTopping1() {
        return topping1;
    }

    public void setTopping1(String topping1) {
        this.topping1 = topping1;
    }

    public String getTopping2() {
        return topping2;
    }

    public void setTopping2(String topping2) {
        this.topping2 = topping2;
    }

    public String getTopping3() {
        return topping3;
    }

    public void setTopping3(String topping3) {
        this.topping3 = topping3;
    }

    public String getTopping4() {
        return topping4;
    }

    public void setTopping4(String topping4) {
        this.topping4 = topping4;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString(){
        if (topping1.equals("none")){
            return pNumber + ". SIZE: " + pSize + ", TOPPINGS: Sauce and Mozzarella | PRICE: " + price;
        }
        else if (topping2.equals("none")){
            return pNumber + ". SIZE: " + pSize + ", TOPPINGS: Sauce, Mozzarella, and " + topping1 + " | PRICE: "  + price;
        }
        else if (topping3.equals("none")){
            return pNumber + ". SIZE: " + pSize + ", TOPPINGS: Sauce, Mozzarella, " + topping1 + ", and " + topping2 + " | PRICE: "  + price;
        }
        else if (topping4.equals("none")){
            return pNumber + ". SIZE: " + pSize + ", TOPPINGS: Sauce, Mozzarella, " + topping1 + ", " + topping2 + ", and " + topping3 + " | PRICE: "  + price;
        }
        else {
            return pNumber + ". SIZE: " + pSize + ", TOPPINGS: Sauce, Mozzarella, " + topping1 + ", " + topping2 + ", " + topping3 + ", and " + topping4 + " | PRICE: "  + price;
        }
    }
}
