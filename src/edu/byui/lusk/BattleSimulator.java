package edu.byui.lusk;

import java.util.*;
public class BattleSimulator {
    Random dieRoll = new Random();
    private String mon1Name;
    private String mon2Name;
    private String mon1Type;
    private String mon2Type;
    private String mon1Weakness;
    private String mon2Weakness;
    private int mon1Health;
    private int mon2Health;
    private int mon1Attack;
    private int mon2Attack;
    private int mon1Defense;
    private int mon2Defense;
    private int mon1Damage;
    private int mon2Damage;
    private int mon1Block;
    private int mon2Block;
    private String monsterAdvantage;
    private boolean mon1Advantage;
    private boolean mon2Advantage;
    private boolean mon1Fainted;
    private boolean mon2Fainted;
    private boolean tieDeclared;
    private boolean winnerDeclared;

    //This constructor is for Testing Purposes, so that the tests do not require user input to build Pokemon objects
    public BattleSimulator(String mon1Name, String mon1Type, String mon1Weakness, int mon1Attack,
                           String mon2Name, String mon2Type, String mon2Weakness, int mon2Attack){
        this.mon1Name = mon1Name;
        this.mon1Type = mon1Type;
        this.mon1Weakness = mon1Weakness;
        this.mon1Attack = mon1Attack;
        this.mon2Name = mon2Name;
        this.mon2Type  = mon2Type;
        this.mon2Weakness = mon2Weakness;
        this.mon2Attack = mon2Attack;

    }

    //This constructor builds a BattleSimulator object from two passed Pokemon Objects
    public BattleSimulator(Pokemon mon1, Pokemon mon2 ){
        mon1Name = mon1.getName();
        mon2Name = mon2.getName();
        mon1Type = mon1.getType();
        mon2Type = mon2.getType();
        mon1Weakness = mon1.getWeakness();
        mon2Weakness = mon2.getWeakness();
        mon1Health = mon1.getHealth();
        mon2Health = mon2.getHealth();
        mon1Attack = mon1.getAttack();
        mon2Attack = mon2.getAttack();
        mon1Defense = mon1.getDefense();
        mon2Defense = mon2.getDefense();
        monsterAdvantage = monsterAdvantage(mon1Type, mon1Weakness, mon2Type, mon2Weakness);

        //This do while loop will call rollDice method enough times until a Pokemon's health is reduced to less than 1
        do {
            mon1Damage = rollDice(mon1Attack);
            mon1Block = rollDice(mon1Defense);

            mon2Damage = rollDice(mon2Attack);
            mon2Block = rollDice(mon2Defense);

            //Damage against mon2 displayed
            if (mon1Damage > mon2Block) {
                mon2Health = mon2Health - (mon1Damage - mon2Block);
                System.out.println(mon2Name + " took " + (mon1Damage - mon2Block) + " damage!");
            }
            else{
                System.out.println(mon2Name + " blocked " + mon1Name + "'s attack!");
            }
            //Damage against mon1 displayed
            if (mon2Damage > mon1Block) {
                mon1Health = mon1Health - (mon2Damage - mon1Block);
                System.out.println(mon1Name + " took " + (mon2Damage - mon1Block) + " damage!");
            }
            else{
                System.out.println(mon1Name + " blocked " + mon2Name + "'s attack!");
            }

            //Checks for if either Pokemon object's health is less than 1
            mon1Fainted = mon1Health < 1;

            mon2Fainted = mon2Health < 1;

            //Returns true if either Pokemon object loses all health
            winnerDeclared = mon2Fainted || mon1Fainted;

            //Stats for the round if neither pokemon fainted
            if(!winnerDeclared){
                System.out.println(mon2Name + "'s health: " + mon2Health + "\n" + mon1Name + "'s health: " + mon1Health);
                try {
                    Thread.sleep(2000);
                }
                catch (InterruptedException e) {

                }
                System.out.println("\nNext Round!");
            }

        }while(!winnerDeclared);

        //Returns true if both pokemon fainted
        tieDeclared = mon1Fainted && mon2Fainted;

        //The winner is pronounced
        if(!tieDeclared){
            if(mon1Fainted){
                System.out.println(mon2Name + " is victorious!");
            }
            else {
                System.out.print(mon1Name + " is victorious!");
            }
        }
        //Results of a tie where both pokemon fainted
        else{
            System.out.println("Both Pokemon fainted! It's a tie! Better luck next time...");
        }

    }

    //Checks for weakness and applies +2 attack to the monster with the type that is the weakness of the other
    public String monsterAdvantage(String monster1Type, String monster1Weakness, String monster2Type, String monster2Weakness){
        String typeAdvantage;
        mon1Advantage = checkWeakness(monster1Type, monster2Weakness);
        mon2Advantage = checkWeakness(monster2Type, monster1Weakness);
        if(mon1Advantage){
            typeAdvantage = mon1Name;
            mon1Attack = mon1Attack + 2;

        }
        else{
            typeAdvantage = mon2Name;
            mon2Attack = mon2Attack + 2;
        }
        //Displays who received +2 attack
        System.out.println(typeAdvantage + " has the advantage!\n");
        monsterAdvantage = typeAdvantage;
        return monsterAdvantage;
    }

    public String getMonsterAdvantage(){
        return monsterAdvantage;
    }

    //Returns true if the one monster's weakness is the same as the other's type
    public boolean checkWeakness(String type,String weakness ){
        if (weakness.equals(type)) {
            return true;
        }
        else{
            return false;
        }
    }

    //This method simulates a 6-sided die being rolled times the number of ATK of the Pokemon object
    //and sums up the results, to be used for damage or protection
    public int rollDice(int numAttackDie){
        int result = 0;

        for (int i = 0; i < numAttackDie; i++) {
            result = result + (dieRoll.nextInt(6) + 1);
        }
        return result;
    }

    //These getters return the different attributes of the created object, for Testing Purposes
    public String getMon1Type(){
        return mon1Type;
    }
    public String getMon2Type(){
        return mon2Type;
    }
    public String getMon1Weakness(){
        return mon1Weakness;
    }
    public String getMon2Weakness(){
        return mon2Weakness;
    }
    public int getMon1Attack(){
        return mon1Attack;
    }
    public int getMon2Attack(){
        return mon2Attack;
    }
}
