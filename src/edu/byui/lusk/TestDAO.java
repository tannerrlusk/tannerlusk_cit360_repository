package edu.byui.lusk;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;


public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO(){
        factory = HibernateMain.getSessionFactory();
    }

    public static TestDAO getInstance(){
        if (single_instance == null){
            single_instance = new TestDAO();
        }
        return single_instance;
    }

    public List<Fruit> getFruit() {
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from edu.byui.lusk.Fruit as f ORDER BY f.continent ";
            List<Fruit> fruitList = (List<Fruit>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return fruitList;
        }
        catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }
        finally {
            session.close();
        }
    }



    public void deleteFruit(String column, String parameter) {
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            String hql = "delete from edu.byui.lusk.Fruit where " + column + " = :" + column;
            Query query = session.createQuery(hql);
            query.setString(column, parameter);
            query.executeUpdate();
            session.getTransaction().commit();
        }
        catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
        }
        finally {
            session.close();
        }
    }

    public Fruit addFruit(String name, String fruitGroup, String regionGrown, String country, String continent, int gramsOfSugar){
        try{
            session = factory.openSession();
            session.getTransaction().begin();
            Fruit newFruit = new Fruit();
            newFruit.setName(name);
            newFruit.setFruitGroup(fruitGroup);
            newFruit.setRegion(regionGrown);
            newFruit.setCountry(country);
            newFruit.setContinent(continent);
            newFruit.setGramsOfSugar(gramsOfSugar);
            session.save(newFruit);
            session.getTransaction().commit();
            return newFruit;
        }
        catch (Exception e){
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        }
        finally {
            session.close();
        }
    }

    public List<Fruit> getUpdatedList(){
        TestDAO t = TestDAO.getInstance();
        List<Fruit> updatedList = t.getFruit();
        return updatedList;
    }



}
