package edu.byui.lusk;

/* The following code was written while following along to the
   youtube videos made by Brian Fraser:
   https://www.youtube.com/watch?v=o5pE7L2tVV8&list=PL-suslzEBiMobIYkpy0ijFzSZF1w_ok8r&index=1
   https://www.youtube.com/watch?v=GaNW6Q__5jc&list=PL-suslzEBiMobIYkpy0ijFzSZF1w_ok8r&index=2
*/

public class Cup {
    private String liquidType;
    private double percentFull;

    public Cup(String liquidType, double percentFull) {
        this.liquidType = liquidType;
        this.percentFull = percentFull;
    }

    public String getLiquidType() {
        return liquidType;
    }

    public void setLiquidType(String liquidType){
       if (liquidType == null) {
            return;
        }
        this.liquidType = liquidType;
    }

    public double getPercentFull(){
        return percentFull;
    }

    public void setPercentFull(double percentFull) {
        if (percentFull > 100 || percentFull < 0){
            throw new IllegalArgumentException("Percent must be >= 0 and <= 100");
        }
        this.percentFull = percentFull;
    }

    public boolean isEmpty(){
        return percentFull == 0;
    }
}
