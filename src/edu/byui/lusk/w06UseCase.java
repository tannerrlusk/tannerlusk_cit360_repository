package edu.byui.lusk;

import java.util.*;


public class w06UseCase {
    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        Scanner input = new Scanner(System.in);
        int numberInput;
        int size = 1;
        int pizzaNumber = 0;
        boolean correctInput = false;
        boolean additionalOrder = true;
        String[] toppings = {"Pepperoni", "Italian Sausage", "Canadian Bacon", "Bacon", "Onions", "Green Bell Peppers", "Pineapple", "Jalapeno Peppers", "Mushrooms", "Olives", "none"};
        String firstTopping = toppings[10];
        String secondTopping = toppings[10];
        String thirdTopping = toppings[10];
        String fourthTopping = toppings[10];
        final double smallBasePrice = 5.00;
        final double mediumBasePrice = 7.00;
        final double largeBasePrice = 9.00;
        final double familyBasePrice = 11.00;
        double currentPizzaBasePrice = 0.00;
        double totalPrice = 0;
        double afterTaxTotal = 0;
        double tax = 1.07;
        String[] sizes = {"Small", "Medium", "Large", "Family"};
        double[] basePrices = {5.00, 7.00, 9.00, 11.00};
        ArrayList<Pizza> pizzaTime = new ArrayList<Pizza>();


        if (hour < 10 || hour >= 23){
            System.out.println("Please place an order during hours of operation.");
            System.out.println("Open from 10:00 AM to 11:30 PM every day.");
            System.exit(0);
        }

        System.out.println("Thank you for choosing Bob's Pizza! Let's get started with the size.");
        do {
            pizzaNumber++;
            do {
                correctInput = false;
                try {
                    System.out.println("\nPlease enter 1 for Small, 2 for Medium, 3 for Large, or 4 for Family");
                    numberInput = input.nextInt();
                    if (numberInput > 0 && numberInput < 5) {
                        correctInput = true;
                        size = numberInput - 1;
                        currentPizzaBasePrice = basePrices[size];
                    } else {
                        System.out.println("Invalid Input");
                        input.nextLine();
                    }

                } catch (InputMismatchException e) {
                    System.out.println("Invalid Input");
                    input.nextLine();
                }
            } while (!correctInput);

            System.out.println("\nNow let's add toppings. Our base pizzas start with mozzarella cheese and our special pizza sauce.");
            System.out.println("Below is a list of available toppings:");
            for (int i = 0; i < toppings.length; i++) {
                System.out.println(i + 1 + "." + toppings[i]);
            }

            do {
                correctInput = false;
                try {
                    System.out.println("Please enter the number of the first topping you'd like to add:");
                    numberInput = input.nextInt();
                    if (numberInput > 0 && numberInput < 12) {
                        firstTopping = toppings[numberInput - 1];
                        correctInput = true;
                    } else {
                        System.out.println("Invalid Input");
                        input.nextLine();
                    }

                } catch (InputMismatchException e) {
                    System.out.println("Invalid Input");
                    input.nextLine();
                }
            } while (!correctInput);

            if (firstTopping.equals(toppings[10])) {
                pizzaTime.add(new Pizza(pizzaNumber, sizes[size], toppings[10], toppings[10], toppings[10], toppings[10], currentPizzaBasePrice));
            } else {
                do {
                    correctInput = false;
                    try {
                        System.out.println("Let's add a second topping");
                        numberInput = input.nextInt();
                        if (numberInput > 0 && numberInput < 12) {
                            secondTopping = toppings[numberInput - 1];
                            correctInput = true;
                        } else {
                            System.out.println("Invalid Input");
                            input.nextLine();
                        }

                    } catch (InputMismatchException e) {
                        System.out.println("Invalid Input");
                        input.nextLine();
                    }
                } while (!correctInput);

                if (secondTopping.equals(toppings[10])) {
                    pizzaTime.add(new Pizza(pizzaNumber, sizes[size], firstTopping, toppings[10], toppings[10], toppings[10], currentPizzaBasePrice));
                } else {
                    do {
                        correctInput = false;
                        try {
                            System.out.println("Let's add a third topping");
                            numberInput = input.nextInt();
                            if (numberInput > 0 && numberInput < 12) {
                                thirdTopping = toppings[numberInput - 1];
                                correctInput = true;
                            } else {
                                System.out.println("Invalid Input");
                                input.nextLine();
                            }

                        } catch (InputMismatchException e) {
                            System.out.println("Invalid Input");
                            input.nextLine();
                        }
                    } while (!correctInput);


                    if (thirdTopping.equals(toppings[10])) {
                        pizzaTime.add(new Pizza(pizzaNumber, sizes[size], firstTopping, secondTopping, toppings[10], toppings[10], currentPizzaBasePrice));
                    } else {
                        do {
                            correctInput = false;
                            try {
                                System.out.println("Let's add a fourth topping");
                                numberInput = input.nextInt();
                                if (numberInput > 0 && numberInput < 12) {
                                    fourthTopping = toppings[numberInput - 1];
                                    correctInput = true;
                                } else {
                                    System.out.println("Invalid Input");
                                    input.nextLine();
                                }

                            } catch (InputMismatchException e) {
                                System.out.println("Invalid Input");
                                input.nextLine();
                            }
                        } while (!correctInput);

                        if (fourthTopping.equals(toppings[10])) {
                            pizzaTime.add(new Pizza(pizzaNumber, sizes[size], firstTopping, secondTopping, thirdTopping, toppings[10], currentPizzaBasePrice));
                        } else {
                            pizzaTime.add(new Pizza(pizzaNumber, sizes[size], firstTopping, secondTopping, thirdTopping, fourthTopping, currentPizzaBasePrice));
                        }
                    }
                }
            }
            System.out.println("How many more orders of that pizza do you want?");
            do {
                correctInput = false;
                try {
                    numberInput = input.nextInt();
                    correctInput = true;
                    for (int i = 0; i < numberInput; i++) {
                        pizzaNumber++;
                        pizzaTime.add(new Pizza(pizzaNumber, sizes[size], firstTopping, secondTopping, thirdTopping, fourthTopping, currentPizzaBasePrice));
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Invalid Input. Please enter how many more orders of that pizza you would like:");
                    input.nextLine();
                }
            } while (!correctInput);

            System.out.println("\nCurrent Order: ");
            for (Pizza pizza : pizzaTime) {
                System.out.println(pizza.toString());
                totalPrice = totalPrice + pizza.getPrice();
            }
            afterTaxTotal = (double)Math.round((totalPrice * tax) * 100)/100;
            System.out.println("Current Total: " + afterTaxTotal);

            System.out.println("Do you want to order another pizza? Enter 1 for YES, 2 for NO.");
            do {
                correctInput = false;
                try {
                    numberInput = input.nextInt();
                    if (numberInput == 1) {
                        correctInput = true;
                    }
                    else if (numberInput == 2){
                        correctInput = true;
                        additionalOrder = false;
                    }
                    else {
                        System.out.println("Invalid Input. Enter 1 to order additional pizzas, 2 to finish ordering.");
                        input.nextLine();
                    }

                } catch (InputMismatchException e) {
                    System.out.println("Invalid Input. Enter 1 to order additional pizzas, 2 to finish ordering.");
                    input.nextLine();
                }
            } while (!correctInput);
        } while(additionalOrder);

        for (Pizza pizza : pizzaTime) {
            System.out.println(pizza.toString());
        }
        System.out.println("Grand Total: " + afterTaxTotal);













    }






}
