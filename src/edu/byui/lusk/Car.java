package edu.byui.lusk;

import java.util.*;

//This class is to be used with w02JavaCollections Class

public class Car {
    private String carModel;
    private int carYear;
    private String carMake;

public Car(String carMake, String carModel, int carYear){
        this.carMake = carMake;
        this.carModel = carModel;
        this.carYear = carYear;
    }

public String toString() {
    return "Make: " + carMake + " Model: " + carModel + " Year: " + carYear;
}

public int getCarYear(){
    return carYear;
}

}
