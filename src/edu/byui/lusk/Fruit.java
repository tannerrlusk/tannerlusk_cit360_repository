package edu.byui.lusk;

import javax.persistence.*;

@Entity
@Table(name = "Fruit")
public class Fruit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "fruitID")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "fruit_group")
    private String fruitGroup;

    @Column(name = "region_grown")
    private String region;

    @Column(name = "country")
    private String country;

    @Column(name = "continent")
    private String continent;

    @Column(name = "gramsOfSugar")
    private int gramsOfSugar;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFruitGroup() {
        return fruitGroup;
    }

    public void setFruitGroup(String fruitGroup) {
        this.fruitGroup = fruitGroup;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public int getGramsOfSugar() {
        return gramsOfSugar;
    }

    public void setGramsOfSugar(int gramsOfSugar) {
        this.gramsOfSugar = gramsOfSugar;
    }

    public Fruit (){

    }

    @Override
    public String toString() {
        return id + ": " + name + ", " + fruitGroup + ", " + gramsOfSugar + "g(s) of sugar, " +
                region + ", " + country + ", " + continent;
    }
}
