package edu.byui.lusk;

import java.util.InputMismatchException;
import java.util.Scanner;

public class w03DataValidation {
    public static void main(String[] args) {
        //Declare variables
        Scanner input = new Scanner(System.in);
        float num1 = 0;
        float num2 = 0;
        float answer = 0;
        boolean correctInput = false;

        //Explanation of program with data validation through Scanner Method
        //will not allow errors to be entered.
        System.out.println("This program will receive two numbers from the user, divide them,");
        System.out.println("and then provide the quotient to the user.");

            do {


                    //Receive inputs and catch if user enters anything
                    //other than a number
                    System.out.println("Please enter a number: ");
                    if(input.hasNextFloat()) {
                        num1 = input.nextFloat();

                        System.out.println("Please enter a second number other than zero: ");
                        if(input.hasNextFloat()) {
                            num2 = input.nextFloat();

                            if(num2 == 0){
                                System.out.println("Second input must not be zero. Let's try again.");
                                input.nextLine();
                            }
                            else {
                                correctInput = true;
                            }
                        }
                        else {
                            System.out.println("Both inputs must be numbers. Let's try again");
                            input.next();
                        }
                    }
                    else {
                        System.out.println("Both inputs must be numbers. Let's try again");
                        input.nextLine();
                    }

            } while (!correctInput);

                answer = returnQuotient(num1, num2);
                System.out.println(answer);



    }

    public static float returnQuotient(float number1, float number2){
        float quotient;
        quotient = number1 / number2;
        return quotient;

    }
}
