package edu.byui.lusk;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class w09HibernateMain {


    public static void main(String[] args) {
        InputMismatchException e = null;
        TestDAO t = TestDAO.getInstance();
        Scanner input = new Scanner(System.in);
        boolean correctInput = false;
        int numEntry = 0;
        int deleteNum = 0;
        boolean c2NumEntry = false;
        boolean repeatAction = false;
        boolean deleteEntry = false;
        String deleteParameter;
        String inputString;
        String columnArray[] = {"fruitID", "name", "fruit_group", "region_grown", "country", "continent", "gramsOfSugar"};


        System.out.println("This program allows the user to view/add/delete entries in the Fruit table " +
                "of the fruits database.");
        do {
            System.out.println("\n\nPlease enter 1 to view Fruit table, 2 to add entry to Fruit table, or 3 to delete entry.");
            try {
                input.hasNextInt();
                numEntry = input.nextInt();
                switch (numEntry) {
                    case 1:
                        List<Fruit> fl = t.getFruit();
                        for (Fruit i : fl) {
                            System.out.println(i.toString());
                        }
                        correctInput = true;
                        break;
                    case 2:
                        System.out.println("\nPlease enter the name of the fruit you are adding:");
                        String fName = input.next();
                        System.out.println("Please enter the fruit group of " + fName);
                        String fGroup = input.next();
                        System.out.println("How many grams of sugar are/in " + fName + "?");
                        int fGramsOfSugar = 0;
                        do {
                            try {
                                input.hasNextInt();
                                fGramsOfSugar = input.nextInt();
                                if (fGramsOfSugar < 1) {
                                    System.out.println("Please enter a number greater than zero: ");
                                    input.nextLine();
                                } else {
                                    c2NumEntry = true;
                                }
                            } catch (InputMismatchException ex) {
                                System.out.println("Please enter a whole number greater than zero:");
                                input.nextLine();
                            }
                        } while (!c2NumEntry);
                        System.out.println("Please enter the region that " + fName + " is grown in:");
                        input.nextLine();
                        String fRegion = input.nextLine();
                        System.out.println("In what country is the " + fRegion + " region located?");
                        String fCountry = input.nextLine();
                        System.out.println("On which continent?");
                        String fContinent = input.nextLine();
                        Fruit newFruit = t.addFruit(fName, fGroup, fRegion, fCountry, fContinent, fGramsOfSugar);
                        System.out.println("--------New Entry -------");
                        System.out.println(newFruit.toString());
                        List<Fruit> ufl = t.getUpdatedList();
                        correctInput = true;
                        break;
                    case 3:
                        System.out.println("\nEnter the parameters to delete by: ");
                        do{
                            System.out.println("1: Delete by ID \n2: Delete by Name \n3: Delete by Group \n" +
                                    "4: Delete by Region \n5: Delete by Country \n6: Delete by Continent" +
                                    "\n7: Delete by Grams of Sugar");
                            try {
                                input.hasNextInt();
                                numEntry = input.nextInt();
                                switch (numEntry){
                                    case 1:
                                         System.out.println("Enter the ID to delete by:");
                                         boolean exitLoop = false;
                                         do {
                                             try {
                                                     deleteNum = input.nextInt();
                                                     deleteParameter = String.valueOf(deleteNum);
                                                     t.deleteFruit(columnArray[numEntry - 1], deleteParameter);
                                                     exitLoop = true;
                                             } catch (InputMismatchException ex){
                                                 System.out.println("Enter a whole number.");
                                                 input.nextLine();
                                             }
                                         }while (!exitLoop);
                                         deleteEntry = true;
                                         break;
                                    case 2:
                                        System.out.println("Enter the Name to delete by: ");
                                        input.nextLine();
                                        deleteParameter = input.nextLine();
                                        t.deleteFruit(columnArray[numEntry-1],deleteParameter);
                                        deleteEntry = true;
                                        break;
                                    case 3:
                                        System.out.println("Enter the Group to delete by: ");
                                        input.nextLine();
                                        deleteParameter = input.nextLine();
                                        t.deleteFruit(columnArray[numEntry-1],deleteParameter);
                                        deleteEntry = true;
                                        break;
                                    case 4:
                                        System.out.println("Enter the Region to delete by: ");
                                        input.nextLine();
                                        deleteParameter = input.nextLine();
                                        t.deleteFruit(columnArray[numEntry-1],deleteParameter);
                                        deleteEntry = true;
                                        break;
                                    case 5:
                                        System.out.println("Enter the Country to delete by: ");
                                        input.nextLine();
                                        deleteParameter = input.nextLine();
                                        t.deleteFruit(columnArray[numEntry-1],deleteParameter);
                                        deleteEntry = true;
                                        break;
                                    case 6:
                                        System.out.println("Enter the Continent to delete by: ");
                                        input.nextLine();
                                        deleteParameter = input.nextLine();
                                        t.deleteFruit(columnArray[numEntry-1],deleteParameter);
                                        deleteEntry = true;
                                        break;
                                    case 7:
                                        System.out.println("Enter the grams of sugar to delete by:");
                                        boolean exitLoop2 = false;
                                        do {
                                            try {
                                                    deleteNum = input.nextInt();
                                                    deleteParameter = String.valueOf(deleteNum);
                                                    t.deleteFruit(columnArray[numEntry - 1], deleteParameter);
                                                    exitLoop2 = true;
                                            } catch (InputMismatchException ex){
                                                System.out.println("Enter a whole number.");
                                                input.nextLine();
                                            }
                                        }while (!exitLoop2);
                                        deleteEntry = true;
                                        break;
                                    default:
                                        System.out.println("Please enter a whole number between 1 and 7");
                                        input.nextLine();
                                        break;
                                }
                            } catch (InputMismatchException ex) {
                                System.out.println("Invalid Input. Enter a whole number between 1 and 7.");
                                input.nextLine();
                            }
                        }while(!deleteEntry);
                        break;


                    default:
                        System.out.println("Please enter either 1, 2, or 3.");
                        input.nextLine();
                        break;
                }
            } catch (InputMismatchException ex) {
                System.out.println("Please enter either 1, 2, or 3.");
                input.nextLine();
            }


            do {
                System.out.println("\nDo you want to perform another action? 1 for Yes, 2 for No.");
                try {
                    input.hasNextInt();
                    numEntry = input.nextInt();
                    switch (numEntry){
                        case 1:
                            correctInput = false;
                            repeatAction = true;
                            break;
                        case 2:
                            correctInput = true;
                            repeatAction = true;
                            break;
                        default:
                            System.out.println("Invalid Input.");
                    }

                } catch (InputMismatchException ex) {
                    System.out.println("Invalid Input. Enter only 1 or 2.");
                    input.nextLine();
                }
            } while (!repeatAction);
            repeatAction = false;

        }while(!correctInput);


        /*
        List<Fruit> newList = t.getUpdatedList();
        System.out.println(newList.toString());


    }



         */
    }

}
