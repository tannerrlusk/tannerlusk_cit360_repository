package edu.byui.lusk;

import java.util.*;

/*
This program was created by Tanner Lusk for CIT360 W02 Assignment.
Write a program that incorporates a list, a queue, a set, and a tree.
Your program should showcase the unique features of these different types of collections.
Your program should demonstrate you know how to use generics with your collections.
Implement the Comparator interface and show how to sort one of your collections.
 */

public class w02JavaCollections {

    public static void main(String[] args) {

        System.out.println("----List interface with Arraylist Implementation----");

        List list = new ArrayList();
            list.add("List");
            list.add("of");
            list.add("same");
            list.add("type");

         for ( Object str : list) {
             System.out.println((String)str);
         }

         List list2 = new ArrayList();
            list2.add(45.0);
            list2.add(45.2);
            list2.add(48.0);

        for (Object db : list2) {
            System.out.println((double)db);
        }

        List list3 = new ArrayList();

        for (int i = 0; i <= 4; i++ ){
            if ( i % 2 == 0){
                list3.add(true);
            }
            else{
                list3.add(false);
            }
        }

        for (Object bool : list3) {
            System.out.println((boolean)bool);
        }

        System.out.println("\n");
        System.out.println("----List interface with LinkedList Implementation----");
        List<Car> cList = new LinkedList<>();
        cList.add(new Car("Toyota", "Corolla", 2010));
        cList.add(new Car("Ford", "Bronco", 2020));
        cList.add(new Car("Cadillac", "Escalade", 2018));

        for (Car cr : cList) {
            System.out.println(cr);
        }


        System.out.println("\n");
        System.out.println("----Queue with Comparator to sort descending by year----");
        Queue<Car> queue = new PriorityQueue<>(3, new CarComparator());
        queue.add(new Car("Ford", "Mustang", 2010));
        queue.add(new Car("Toyota", "Avalon", 2020));
        queue.add(new Car("Acura", "MDX", 2015));

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()){
            System.out.println(queue.poll());
        }

        System.out.println("\n");
        System.out.println("----Tree Set----");
        Set setOfTreeTypes = new TreeSet();
        setOfTreeTypes.add("Oak");
        setOfTreeTypes.add("Oak");
        setOfTreeTypes.add("Aspen");
        setOfTreeTypes.add("aspen");
        setOfTreeTypes.add("Pine");
        setOfTreeTypes.add("Pine");
        setOfTreeTypes.add("blue spruce");

        for (Object str : setOfTreeTypes){
            System.out.println((String)str);
        }

        System.out.println("\n");
        System.out.println("----Hash Set----");
        Set setofHashBrowns = new HashSet();
        setofHashBrowns.add("Perfect");
        setofHashBrowns.add("Soggy");
        setofHashBrowns.add("Burnt");
        setofHashBrowns.add("Perfect");

        for (Object str: setofHashBrowns){
            System.out.println((String)str);
        }

        System.out.println("\n");
        System.out.println("----Map----");
        Map map = new HashMap();
        map.put(1,"Rexburg");
        map.put(2,"Rigby");
        map.put(3,"Ashton");
        map.put(4,"Idaho Falls");
        map.put(3,"Menan");

        for (int i = 1; i < 7; i++){
            String city = (String)map.get(i);
            System.out.println(city + ", ID");
        }

    }

}
