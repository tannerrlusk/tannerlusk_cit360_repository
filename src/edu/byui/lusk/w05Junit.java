package edu.byui.lusk;

import java.util.*;
public class w05Junit {


    public static void main(String[] args) {

        //Explains what the user will be doing and begins the process of creating the first pokemon
        System.out.println("This program will create two Pokemon objects and will simulate a battle between them.\n");
        System.out.println("You will now create your first Pokemon.");
        Pokemon mon1 = new Pokemon();

        System.out.println(mon1.toString());

        //Begins the process of creating the second pokemon
        System.out.println("\n\nYou will now create your second Pokemon.");
        Pokemon mon2 = new Pokemon();

        System.out.println(mon2.toString());

        //Dramatic effect...
        try {
            Thread.sleep(2000);
        }
        catch (InterruptedException e) {

        }
        System.out.print("\n\n" + mon1.getName()+"...");
        try {
            Thread.sleep(2000);
        }
        catch (InterruptedException e) {

        }
        System.out.print(mon2.getName()+"...");
        try {
            Thread.sleep(2000);
        }
        catch (InterruptedException e) {

        }
        System.out.println("Let's battle!");
        //Passes the newly created pokemon objects to create a BattleSimulator object that will determine a winner
        BattleSimulator battle = new BattleSimulator(mon1, mon2);
    }









}
