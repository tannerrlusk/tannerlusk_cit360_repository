package edu.byui.lusk;


//This class is for use with w02JavaCollections.java class
import java.util.*;

class CarComparator implements Comparator<Car> {
    @Override
    public int compare(Car o1, Car o2) {
        if (o1.getCarYear() > o2.getCarYear()){
            return -1;
        }
        else if (o1.getCarYear() < o2.getCarYear()){
            return 1;
        }
        return 0;
    }
}
