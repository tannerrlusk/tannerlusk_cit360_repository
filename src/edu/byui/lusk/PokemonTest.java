package edu.byui.lusk;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PokemonTest {

    //These tests assert that when an object is built, the attributes are correctly assigned
    @Test
    void getName() {
        Pokemon monster = new Pokemon("Charizard", "Fire", "Water", 30, 5,5 );
        assertEquals("Charizard",monster.getName());
    }

    @Test
    void getType() {
        Pokemon monster = new Pokemon("Charizard", "Fire", "Water", 30, 5,5 );
        assertEquals("Fire",monster.getType());
    }

    @Test
    void getWeakness() {
        Pokemon monster = new Pokemon("Charizard", "Fire", "Water", 30, 5,5 );
        assertEquals("Water",monster.getWeakness());
    }

    @Test
    void getHealth() {
        Pokemon monster = new Pokemon("Charizard", "Fire", "Water", 30, 5,5 );
        assertEquals(30,monster.getHealth());
    }

    @Test
    void getAttack() {
        Pokemon monster = new Pokemon("Charizard", "Fire", "Water", 30, 5,5 );
        assertEquals(5,monster.getAttack());
    }

    @Test
    void getDefense() {
        Pokemon monster = new Pokemon("Charizard", "Fire", "Water", 30, 5,4 );
        assertEquals(4 ,monster.getDefense());
    }

    @Test
    void testToString() {
        Pokemon monster = new Pokemon("Charizard", "Fire", "Water", 30, 5,4 );
        assertEquals("Charizard is a Fire type Pokemon with 5 ATK, 4 DEF, and 30 health.\nIts weakness is Water.", monster.toString());
    }
}