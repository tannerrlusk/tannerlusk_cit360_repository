package edu.byui.lusk;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BattleSimulatorTest {


    @Test
    void monsterAdvantage() {
        BattleSimulator bl = new BattleSimulator("Charizard", "Fire", "Water", 5,
                                                 "Squirtle", "Water", "Plant", 4);
        bl.monsterAdvantage(bl.getMon1Type(),bl.getMon1Weakness(),bl.getMon2Type(),bl.getMon2Weakness());
        assertEquals(6, bl.getMon2Attack());
        assertEquals(5, bl.getMon1Attack());
        assertEquals("Squirtle", bl.getMonsterAdvantage());
    }

    @Test
    void checkWeakness() {
        BattleSimulator bl = new BattleSimulator("Charizard", "Fire", "Water", 5,
                "Squirtle", "Water", "Plant", 4);
        assertTrue(bl.checkWeakness(bl.getMon2Type(),bl.getMon1Weakness()));
        assertFalse(bl.checkWeakness(bl.getMon1Type(),bl.getMon2Weakness()));

    }

    @Test
    void rollDice() {
        int attack;
        int result;
        BattleSimulator bl = new BattleSimulator("Charizard", "Fire", "Water", 5,
                "Squirtle", "Water", "Plant", 4);
        attack = bl.getMon1Attack();
        result = bl.rollDice(attack);
        assertTrue(result > 0);
        assertTrue(result <31);
    }
}