package edu.byui.lusk;
import java.util.*;

public class w03ExceptionHandling {

    public static void main(String[] args) {
        //Declare variables
        Scanner input = new Scanner(System.in);
        float num1 = 0;
        float num2 = 0;
        float answer = 0;
        boolean correctInput = false;

        //Explanation of program and errors will be caught through exception handling
        System.out.println("This program will receive two numbers from the user, divide them,");
        System.out.println("and then provide the quotient to the user.");

        do {
            do {

                try {
                    //Receive inputs and catch if user enters anything
                    //other than a number
                    System.out.println("Please enter a number: ");
                    num1 = input.nextFloat();

                    System.out.println("Please enter a second number other than zero: ");
                    num2 = input.nextFloat();
                    correctInput = true;
                } catch (InputMismatchException e) {
                    System.out.println("Both inputs must be numbers. Let's try again");
                    input.nextLine();
                }
            } while (!correctInput);

            //Reset Boolean
            correctInput = false;

            try {
                answer = returnQuotient(num1, num2);
                correctInput = true;
            } catch (ArithmeticException e) {
                System.out.println("The second number must not be zero. Let's try again");
                input.nextLine();
            }
        } while(!correctInput);

        System.out.println(answer);



    }

    public static float returnQuotient(float number1, float number2)throws ArithmeticException{
        float quotient;
        if (number2 == 0){
            throw new ArithmeticException("Cannot divide by zero");
        }
        else {
            quotient = number1 / number2;
            return quotient;
        }
    }
}
