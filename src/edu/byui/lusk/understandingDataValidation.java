package edu.byui.lusk;

import java.util.*;


public class understandingDataValidation {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String fName;
        String lowerName;
        int age = 0;
        boolean validFName = true;
        boolean validNumber = true;

        do {
            // Receive input for first name
            System.out.print("Please enter the first name: ");
            fName = input.next();
            lowerName = fName.toLowerCase();
            char[] charArray = lowerName.toCharArray();
            //Verify that name contains only letters
            for (char ch : charArray) {
                if (!(ch >= 'a' && ch <= 'z')) {
                    validFName = false;
                    System.out.println("First name can only contain letters A-Z");
                    break;
                }
                else {
                    validFName = true;
                }
            }
        } while (!(validFName));

        do{
            System.out.print("Please enter " + fName + "'s age: ");
            if (input.hasNextInt()){
                age = input.nextInt();
                validNumber = true;

            }
            else {
                System.out.println("Please enter a whole number.");
                validNumber = false;
                input.next();
            }
        } while (!(validNumber));

        System.out.println("Name: " + fName + "\nAge: " + age + "\nThank you!");






    }
}
