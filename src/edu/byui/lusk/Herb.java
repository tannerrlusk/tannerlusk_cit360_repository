package edu.byui.lusk;

public class Herb implements Runnable {

    private String name;
    private int currentHeight = 0;
    private int maturePlant;
    private int growthRate;

    public Herb (String herbName){

        switch (herbName){
            case "Oregano":
                name = herbName;
                maturePlant = 60;
                growthRate = 7;
                break;
            case "Parsley" :
                name = herbName;
                maturePlant = 45;
                growthRate = 9;
                break;
            case "Cilantro" :
                name = herbName;
                maturePlant = 60;
                growthRate = 10;
                break;
            case "Rosemary" :
                name = herbName;
                maturePlant = 90;
                growthRate = 12;
                break;
            case "Basil" :
                name = herbName;
                maturePlant = 15;
                growthRate = 8;
                break;
        }

    }

    public void run(){
        try
        {

            do{
                currentHeight = currentHeight+growthRate;
                //Display to user current height of herb
                System.out.println(name + " is " + currentHeight + " cm tall");
                Thread.sleep(750);
            } while (currentHeight < maturePlant);
            System.out.println(name + " is " + currentHeight + " centimeters tall and is ready to be harvested!");
        }
        catch (InterruptedException e) {
            e.printStackTrace();;
        }
    }
}
