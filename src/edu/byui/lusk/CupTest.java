package edu.byui.lusk;

/* The following code was written while following along to the
   youtube videos made by Brian Fraser:
   https://www.youtube.com/watch?v=o5pE7L2tVV8&list=PL-suslzEBiMobIYkpy0ijFzSZF1w_ok8r&index=1
   https://www.youtube.com/watch?v=GaNW6Q__5jc&list=PL-suslzEBiMobIYkpy0ijFzSZF1w_ok8r&index=2
*/

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CupTest {
    @Test
    void testObjectCreation() {
        Cup cup = new Cup("Cherry Pepsi", 75.0);
        assertEquals("Cherry Pepsi", cup.getLiquidType());
        assertEquals(75, cup.getPercentFull(), 0.001);
    }

    @Test
    void testObjectCreationWithAssertAll() {
        Cup cup = new Cup("Cherry Pepsi", 75.0);
        assertAll("Correctly builds object",
                () -> assertEquals("Cherry Pepsi", cup.getLiquidType()),
                () -> assertEquals(75, cup.getPercentFull(), 0.001)
        );
    }

    @Test
    void testIsEmpty() {
        Cup cup = new Cup("Cherry Pepsi", 75.0);
        assertFalse(cup.isEmpty());
    }

    @Test
    void testSetLiquidWithNull(){
        Cup cup = new Cup("Cherry Pepsi", 75.0);
        cup.setLiquidType(null);
        assertNotNull(cup.getLiquidType());
    }

    @Disabled("This was disabled for this reason")
    @Test
    void testExternalLibrary(){
        // Imagine depending on someone else's code...
        // ...
        fail();
    }

    @Test
    void testSetBadPercentThrows(){ Cup cup = new Cup("Cherry Pepsi", 75.0);
        assertThrows(IllegalArgumentException.class, () -> cup.setPercentFull(-1));
    }



    @Test
    void getLiquidType() {
        Cup c = new Cup("Orange Juice", 85.5);
        assertEquals("Orange Juice", c.getLiquidType());
    }
    @Test
    void getPercentageFull() {
        Cup c = new Cup("Orange Juice", 85.5);
        assertEquals(85.5, c.getPercentFull(), 0.001);
    }

    @Test
    void setLiquidType() {
        Cup c = new Cup("Mountain Dew", 99.05);
        c.setLiquidType("Boring Water");
        assertEquals("Boring Water", c.getLiquidType());
    }

    @Test
    void setPercentFull() {
        Cup c = new Cup("Mountain Dew", 99.05);
        c.setPercentFull(50.05);
        assertEquals(50.05, c.getPercentFull());
    }
}