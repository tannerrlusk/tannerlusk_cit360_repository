package edu.byui.lusk;

import java.util.*;

public class Pokemon {
    private String name;
    private String type;
    private String weakness;
    private int health;
    private int attack;
    private int defense;
    Scanner input = new Scanner(System.in);

    //This constructor is for Testing purposes so that the tests will not require user input
    public Pokemon(String name, String type, String weakness, int health, int attack, int defense){
        this.name = name;
        this.type = type;
        this.weakness = weakness;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
    }

    //This constructor calls methods to build the Pokemon object according to user input.
    public Pokemon(){
        setName();
        setType();
        setWeakness();
        setHealth();
        setAttack();
        setDefense();
    }


    public void setName(){
        System.out.println("Please enter the Pokemon's name: ");
        name = input.nextLine();
    }
    public String getName(){
        return name;
    }


    //This method will collect input from the user, and verify they chose one of the three types.
    public void setType(){
        boolean correctType;
        do{
            System.out.println("Enter Fire, Water, or Plant for " + name + "'s type: ");
            type = input.nextLine();

            switch (type) {
                case "Fire", "Plant", "Water" -> correctType = true;
                default -> {
                    correctType = false;
                    System.out.println("Type must be either Fire, Water, or Plant!");
                }
            }

        }while(!correctType);
    }
    public String getType(){
        return type;
    }


    //This method sets the Pokemon object's weakness, depending on the type
    public void setWeakness(){
        if (type.equals("Fire")) {
            weakness = "Water";
        }
        else if(type.equals("Water")){
            weakness = "Plant";
        }
        else{
            weakness = "Fire";
        }
    }
    public String getWeakness(){
        return weakness;
    }


    //This method lets the user input a positive whole number for the health
    public void setHealth(){
        boolean isInteger = false;
        do {
            System.out.println("Enter the health:");
            try {
                health = input.nextInt();
                if (health > 0) {
                    isInteger = true;
                } else {
                    isInteger = false;
                    System.out.println("Number must be greater than zero.");
                    input.nextLine();
                }
            } catch (InputMismatchException e) {
                System.out.println("Please enter a whole number.");
                input.nextLine();
            }
        }while(!isInteger);
    }
    public int getHealth(){
        return health;
    }

    //This method determines the attack, or the amount of rolls they'll get
    //Entries must be a positive, whole number
    public void setAttack() {
        boolean isInteger = false;
        do {
            System.out.println("Enter the attack:");
            try {
                attack = input.nextInt();
                if (attack > 0){
                    isInteger = true;
                }
                else{
                    isInteger = false;
                    System.out.println("Number must be greater than zero.");
                    input.nextLine();
                }
            }
            catch(InputMismatchException e){
                System.out.println("Please enter a whole number.");
                input.nextLine();
            }

        }while(!isInteger);
    }
    public int getAttack() {
        return attack;
    }

    public int getDefense() {
        return defense;
    }
    //This method determines the defense, or the amount of rolls they'll get
    //Entries must be a positive, whole number
    public void setDefense() {
        boolean isInteger = false;
        do {
            System.out.println("Enter the defense:");
            try {
                defense = input.nextInt();
                if (defense > 0){
                    isInteger = true;
                }
                else{
                    isInteger = false;
                    System.out.println("Number must be greater than zero.");
                    input.nextLine();
                }
            }
            catch(InputMismatchException e){
                System.out.println("Please enter a whole number.");
                input.nextLine();
            }

        }while(!isInteger);

    }

    //This method returns a String overview of the Pokemon object to the user.
    public String toString(){
        return name + " is a " + type + " type Pokemon with " + attack + " ATK, " + defense + " DEF, and " + health + " health.\nIts weakness is " + weakness + ".";
    }


}
